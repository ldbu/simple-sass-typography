/*********************************************
 *  Simple Sass Typography
 *********************************************/

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    gulp.src('sass/**/*.scss')
        .pipe(sass({outputStyle: 'nested'}).on('error', sass.logError))
        .pipe(gulp.dest('dist/'))
});

gulp.task('default',function() {
    gulp.watch('sass/**/*.scss',['styles']);
});