# Simple SASS Typography

Simple SASS Typography provides a few simple color schemes for basic markdown typography.

## Example Dracula Css

Based [loosely] on the excellent [Dracula theme](https://draculatheme.com/).

![Screenshot](screenshot.png)